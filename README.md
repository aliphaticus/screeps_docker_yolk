# Screeps Docker Yolk



## Info

Screeps dedicated server intended for use with the pterodactyl panel

The following ENV needs to be set:
- STEAM_API_KEY (retrieve from https://steamcommunity.com/dev/apikey)

The following ENVs can be modified:
- SERVER_PORT <port>
- SERVER_PASSWORD <password>
- RUNNER_THREADS <num_threads> (do not exceed physical cpus)
- PROCESSORS_CNT<num_processors> (do not exceed physical cpus)
